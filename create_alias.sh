#!/bin/bash

COMMAND="$(which python) $(pwd)/imgtotxt.py"

# Create alias for atual session - only works with source command. Ex: 'source ./create_alias.sh'
alias imgtotxt="$COMMAND"


# Append the command for create alias in ~/.bashrc
if ! grep "^alias imgtotxt" ~/.bashrc > /dev/null
then 
    echo -e "\n# Add alias for imgtotxt\nalias imgtotxt='$COMMAND'\n" >> ~/.bashrc; 
fi


