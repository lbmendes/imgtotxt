import pytesseract
from PIL import Image, ImageOps

import custom_parser


def main():
    args = custom_parser.set_args()

    print(f"List of params: {args._get_kwargs()}")
    print(" Begin of text output  ".center(50, "*"))

    with Image.open(args.image_file) as image:

        if args.invert_color:
            inverted_image = ImageOps.invert(image)
            image = inverted_image

        if args.grayscale:
            grayscale_image = image.convert("L")
            image = grayscale_image

        print(pytesseract.image_to_string(image, lang=args.lang))

    print(" End of text output ".center(50, "*"))


if __name__ == "__main__":
    main()
